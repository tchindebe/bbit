import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App flex h-full w-full items-center justify-center  bg-gray-200">
    <h1 className="text-3xl font-bold underline">
      Hello world!
    </h1>
    </div>
  );
}

export default App;
